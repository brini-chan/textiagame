package ur.mi.textia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TextiaDB {

	private static final String DATABASE_NAME = "textiadb.db";
	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_TABLE = "playerscores";
	public static final String KEY_ID = "_id";
	public static final String KEY_NAME = "name";
	public static final String KEY_PROGRESS = "progress";
	public static final String KEY_IMAGES = "images";
	public static final String KEY_STORY = "story";

	public static final int COLUMN_ID_INDEX = 0;
	public static final int COLUMN_NAME_INDEX = 1;
	public static final int COLUMN_PROGRESS_INDEX = 2;
	public static final int COLUM_IMAGES_INDEX = 3;
	public static final int COLUM_STORY_INDEX = 4;

	private TextiaDBOpenHelper dbHelper;

	private SQLiteDatabase db;

	public TextiaDB(Context context) {
		dbHelper = new TextiaDBOpenHelper(context, DATABASE_NAME, null,
				DATABASE_VERSION);
	}

	private class TextiaDBOpenHelper extends SQLiteOpenHelper {

		// HEEEEEEEEEEELP!
		private static final String DATABASE_CREATE = "create table "
				+ DATABASE_TABLE + " (" + KEY_ID
				+ " integer primary key autoincrement, " + KEY_NAME
				+ " text not null, " + KEY_PROGRESS + " text, " + KEY_IMAGES
				+ " integer," + KEY_STORY + " text);";

		public TextiaDBOpenHelper(Context c, String dbname,
				SQLiteDatabase.CursorFactory factory, int version) {
			super(c, dbname, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		}
	}

	public void open() throws SQLException {
		try {
			db = dbHelper.getWritableDatabase();
		} catch (SQLException e) {
			db = dbHelper.getReadableDatabase();
		}
	}

	public void close() {
		db.close();
	}

	public long insertPlayerHighscores(String playerName, int progress, String story, int images) {

		ContentValues newToDoValues = new ContentValues();

		newToDoValues.put(KEY_NAME, playerName);
		newToDoValues.put(KEY_PROGRESS, progress);
		newToDoValues.put(KEY_IMAGES, images);
		newToDoValues.put(KEY_STORY, story);

		return db.insert(DATABASE_TABLE, null, newToDoValues);
	}

	public Cursor getAllHighscores() {
		return db.query(DATABASE_TABLE, new String[] { KEY_ID, KEY_NAME,
				KEY_PROGRESS, KEY_IMAGES, KEY_STORY }, null, null, null, null, null);

	}

}
