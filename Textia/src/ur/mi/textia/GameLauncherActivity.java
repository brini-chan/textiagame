package ur.mi.textia;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;

public class GameLauncherActivity extends Activity {
	private LauncherThread launcherThread;
	private MediaPlayer mediaPlayer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_launcher);
		setupMediaPlayer();
		startThread();
	}

	private void setupMediaPlayer() {
		mediaPlayer= MediaPlayer.create(this, R.raw.launchersound);
		mediaPlayer.start();
	}

	private void startThread() {
		launcherThread=new LauncherThread();
		launcherThread.start();
	}
	
	class LauncherThread extends Thread implements Runnable{
		public void run(){
			waitAndStartIntent();
		}

		private void waitAndStartIntent() {
			try {
				sleep(5000);
				Intent intent=new Intent(GameLauncherActivity.this, GameMenuActivity.class);
				startActivity(intent);
				finish();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			
		}
	}
}
