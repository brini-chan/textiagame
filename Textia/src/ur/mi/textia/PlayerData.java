package ur.mi.textia;



public class PlayerData {
	
	private String playerName;
	private int progress;
	private String storyList;
	private int imageList;
	
	public PlayerData(String playerName, int progress, String storyList, int imageList){
		this.playerName=playerName;
		this.progress=progress;
		this.storyList=storyList;
		this.imageList=imageList;
	}
	
	public String getPlayerName(){
		return playerName;
	}
	
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	public int getProgress(){
		return progress;
	}
	
	public void setChapterCount(int progress) {
		this.progress = progress;
	}
	
	public String getStory(){
		return storyList;
	}
	
	public void setStoryList(String storyList) {
		this.storyList = storyList;
	}
	
	public int getImages(){
		return imageList;
	}
	
	public void setImages(int imageList) {
		this.imageList = imageList;
	}
	

}
