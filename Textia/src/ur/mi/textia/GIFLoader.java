package ur.mi.textia;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;



public class GIFLoader extends ImageView {
	
	public GIFLoader(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public GIFLoader(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GIFLoader(Context context) {
        super(context);
        init();
    }

    private void init() {
        setBackgroundResource(R.drawable.gifframes);
        final AnimationDrawable frameAnimation = (AnimationDrawable) getBackground();
        post(new Runnable(){
            public void run(){
                 frameAnimation.start();
             }
        });
    }
}



