package ur.mi.textia;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class ActOneActivity extends Activity {
	private ImageView chapterImage;
	private ImageView splashImage;
	private TextView storyView;
	private Button buttonLeft;
	private Button buttonMiddle;
	private Button buttonRight;
	private String currentOption;
	private MediaPlayer mediaPlayer;
	private ImageButton muteButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chapter_one);
		referenceUI();
		playSound(0);
		setupButtonLeftFunction();
		setupButtonMiddleFunction();
		setupMuteButton();

	}

	private void setupMuteButton() {
		muteButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mediaPlayer.isPlaying()) {
					mediaPlayer.pause();
					muteButton.setImageResource(R.drawable.playsoundbutton);
				} else {
					mediaPlayer.start();
					muteButton.setImageResource(R.drawable.mutebutton);
				}

			}
		});

	}

	private void playSound(int textID) {
		int sound = 0;
		switch (textID) {
		case 0:
			sound = R.raw.startstory;
			break;
		case 1:
			sound = R.raw.rauchfahne;
			break;
		// TODO more cases
		case 1000:
			sound = R.raw.bloodsplatter;
			break;
		}
		mediaPlayer = MediaPlayer.create(this, sound);
		mediaPlayer.start();
	}

	private void referenceUI() {

		muteButton = (ImageButton) findViewById(R.id.mute_button);
		chapterImage = (ImageView) findViewById(R.id.game_image);
		splashImage = (ImageView) findViewById(R.id.splash);
		storyView = (TextView) findViewById(R.id.main_textview);
		buttonLeft = (Button) findViewById(R.id.buttonLeft);
		buttonMiddle = (Button) findViewById(R.id.buttonMiddle);
		buttonRight = (Button) findViewById(R.id.buttonRight);
		storyView.setMovementMethod(new ScrollingMovementMethod());
		initTypeface();
	}

	private void setupButtonLeftFunction() {
		buttonLeft.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				currentOption = buttonLeft.getText().toString();
				// Story f�r Button-Event "Rauchfahne" (Ebene 1)
				if (currentOption.equals(getString(R.string.rauchfahne))) {
					mediaPlayer.stop();
					storyView.setText(getString(R.string.story_rauchfahne));
					playSound(1);
					chapterImage.setImageResource(R.drawable.wagon);
					storyView.scrollTo(0, 0);
					buttonLeft.setText(getString(R.string.wagen));
				}

				// Story f�r Button-Event "Wagen" (Ebene 2)
				if (currentOption.equals(getString(R.string.wagen))) {
					storyView.setText(getString(R.string.story_wagen));
					storyView.scrollTo(0, 0);
					mediaPlayer.stop();
					chapterImage.setImageResource(R.drawable.sterbendermann);
					buttonLeft.setText(getString(R.string.sohn));
					buttonMiddle.setVisibility(View.VISIBLE);
					buttonMiddle.setText(getString(R.string.banditen));
				}

				// Story f�r Button-Event "Sohn" (Ebene 3)
				if (currentOption.equals(getString(R.string.sohn))) {
					storyView.setText(getString(R.string.story_sohn));
					storyView.scrollTo(0, 0);
					chapterImage.setImageResource(R.drawable.rankenstein);
					buttonLeft.setText(getString(R.string.stein));
					buttonMiddle.setVisibility(View.VISIBLE);
					buttonMiddle.setText(getString(R.string.ranken));
				}

				// Story f�r Button-Event "Stein" (Ebene 4)
				if (currentOption.equals(getString(R.string.stein))) {
					storyView.setText(getString(R.string.story_stein));
					storyView.scrollTo(0, 0);
					buttonLeft.setText("done");
					buttonLeft.setVisibility(View.INVISIBLE);
				}
			}
		});
	}

	private void setupButtonMiddleFunction() {
		buttonMiddle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				currentOption = buttonMiddle.getText().toString();

				// Story f�r Button-Event "Banditen" (Ebene 3)
				if (currentOption.equals(getString(R.string.banditen))) {
					storyView.setText(getString(R.string.story_banditen));
					storyView.scrollTo(0, 0);
					buttonMiddle.setVisibility(View.INVISIBLE);
				}

				// Story f�r "Button"-Event Ranken ohne vorher "Stein"-Event
				// (Ebene 4)
				if (currentOption.equals(getString(R.string.ranken))
						&& buttonLeft.getText().toString().equals("done")) {
					storyView.setText(getString(R.string.story_ranken));
					storyView.scrollTo(0, 0);
					buttonMiddle.setText(getString(R.string.klettern));
					buttonLeft.setVisibility(View.VISIBLE);
					buttonLeft.setText(getString(R.string.schwert));
				}

				// Story f�r Button-Event "Ranken" mit vorherigen "Stein"-Event
				// (Ebene 4)
				if (currentOption.equals(getString(R.string.ranken))
						&& buttonLeft.getText().toString()
								.equals(getString(R.string.stein))) {
					Intent intent = new Intent(ActOneActivity.this,
							GameOverActivity.class);
					String deathMessage = getString(R.string.ranken_death_message);
					intent.putExtra("rankenTod", deathMessage);
					disableInteraction();
					startActivity(intent);
				}
			}

			private void disableInteraction() {
				buttonLeft.setVisibility(View.INVISIBLE);
				buttonMiddle.setVisibility(View.INVISIBLE);
				chapterImage.setVisibility(View.INVISIBLE);
				splashImage.setVisibility(View.VISIBLE);
				playSound(1000);
				storyView.setText(" ");

			}
		});
	}

	private void initTypeface() {
		Typeface typeface = Typeface.createFromAsset(getAssets(),
				"fonts/FingerPaint-Regular.ttf");
		storyView.setTypeface(typeface);
		buttonLeft.setTypeface(typeface);
		buttonMiddle.setTypeface(typeface);
		buttonRight.setTypeface(typeface);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		//TODO!!
		if ((keyCode == KeyEvent.KEYCODE_HOME)) {
			mediaPlayer.stop();
			return true;
		}
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			mediaPlayer.pause();
			popUpDialog();
			return super.onKeyDown(keyCode, event);
		}
		if ((keyCode == KeyEvent.KEYCODE_MENU)) {
			return true;
		}
		return false;
	}

	private void popUpDialog() {
		 AlertDialog.Builder builder = new AlertDialog.Builder(this);
	        builder.setMessage("M�chtest du deine Reise wirklich beenden?")
	              .setCancelable(true)
	               .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       dialog.cancel();
	                       mediaPlayer.stop();
	                       finish();
	                   }
	               })
	               .setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                        dialog.cancel();
	                        mediaPlayer.start();
	                   }
	               });
	        AlertDialog alert = builder.create();
	        alert.setTitle("Beenden?");
	        alert.show();
		
	}

}
