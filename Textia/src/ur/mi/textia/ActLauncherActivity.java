package ur.mi.textia;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;



public class ActLauncherActivity extends Activity {
	private LauncherThread launcherThread;
	private MediaPlayer mediaPlayer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_akt1_launcher);
		setupMediaPlayer();
		startThread();
	}

	private void startThread() {
		launcherThread=new LauncherThread();
		launcherThread.start();
		
	}
	
	private void setupMediaPlayer() {
		mediaPlayer= MediaPlayer.create(this, R.raw.gong);
		mediaPlayer.start();
	}
	
	class LauncherThread extends Thread implements Runnable{
		public void run(){
			waitAndStartIntent();
		}

		private void waitAndStartIntent() {
			try {
				sleep(2500);
				Intent intent=new Intent(ActLauncherActivity.this, ActOneActivity.class);
				overridePendingTransition(0, 0);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				finish();
				overridePendingTransition(0, 0);
				startActivity(intent);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			
		}
	}
}
