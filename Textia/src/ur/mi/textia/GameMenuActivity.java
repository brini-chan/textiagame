package ur.mi.textia;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class GameMenuActivity extends Activity {
	
	private Button startButton;
	private Button storiesButton;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu_screen);
		setupUI();
		setupButtonListener();
	}


	private void setupButtonListener() {
		startButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent= new Intent(GameMenuActivity.this,ActLauncherActivity.class);
				startActivity(intent);
				finish();
				
			}
		});
		
		storiesButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent= new Intent(GameMenuActivity.this, StoriesActivity.class);
				startActivity(intent);
				
			}
		});
		
	}
	
	private void setupUI() {
		startButton=(Button)findViewById(R.id.start_button);
		storiesButton=(Button)findViewById(R.id.stories_button);
		initTypeface();
	}
	
	private void initTypeface() {
		Typeface typeface = Typeface.createFromAsset(getAssets(),
				"fonts/FingerPaint-Regular.ttf");
		startButton.setTypeface(typeface);
		storiesButton.setTypeface(typeface);

	}

}
