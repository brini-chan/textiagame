package ur.mi.textia;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class GameOverActivity extends Activity {

	private TextView gameOverTitle;
	private TextView gameOverDescription;
	private EditText nameField;
	private Button addStory;
	private Button skipStory;
	private String deathMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_over);
		retrieveValues();
		dimBackground();
		setupUI();
		setupButtonFunctions();
	}

	private void setupButtonFunctions() {
		skipStory.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(GameOverActivity.this,
						GameMenuActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});

		addStory.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if ((nameField.getText().toString()).equals("")) {
					startAnonymousToast();
				} else {
					sendToast();
					Intent intent = new Intent(GameOverActivity.this,
							GameMenuActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
				}
			}

			private void startAnonymousToast() {
				Context context = getApplicationContext();
				int duration = Toast.LENGTH_SHORT;
				Toast toast = Toast.makeText(context,
						getString(R.string.toast_anonymous), duration);
				toast.show();

			}

			private void sendToast() {
				Context context = getApplicationContext();
				int duration = Toast.LENGTH_SHORT;
				Toast toast = Toast.makeText(context,
						getString(R.string.toast_message), duration);
				toast.show();

			}
		});

	}

	private void setupUI() {
		gameOverTitle = (TextView) findViewById(R.id.gameover_message);
		gameOverDescription = (TextView) findViewById(R.id.gameover_description);
		nameField = (EditText) findViewById(R.id.game_over_edittext);
		addStory = (Button) findViewById(R.id.add_button);
		skipStory = (Button) findViewById(R.id.skip_button);
		initTypeface();
		gameOverDescription.setText(deathMessage);
	}

	private void initTypeface() {
		Typeface typeface = Typeface.createFromAsset(getAssets(),
				"fonts/FingerPaint-Regular.ttf");
		gameOverTitle.setTypeface(typeface);
		gameOverDescription.setTypeface(typeface);
		addStory.setTypeface(typeface);
		skipStory.setTypeface(typeface);
	}

	private void retrieveValues() {
		Bundle e = getIntent().getExtras();
		deathMessage = e.getString("rankenTod");

	}

	private void dimBackground() {
		Window mainWindow = getWindow();
		int dim = 80;
		mainWindow.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		WindowManager.LayoutParams params = mainWindow.getAttributes();
		params.dimAmount = dim / 100f;
		mainWindow.setAttributes(params);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}
