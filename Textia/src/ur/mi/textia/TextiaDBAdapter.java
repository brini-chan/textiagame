package ur.mi.textia;

import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TextiaDBAdapter extends ArrayAdapter<PlayerData> {

		private List<PlayerData> playerList;
		private Context context;
		private Drawable storyDrawable;

		public TextiaDBAdapter(Context context,
				List<PlayerData> playerDataList) {
			super(context, R.id.highscore_item);
			this.context = context;
			this.playerList = playerDataList;

		}

		@Override
		public int getCount() {
			return playerList.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = inflater.inflate(R.layout.textia_playerdata_listitem, null);
			}

			PlayerData playerItem = playerList.get(position);

			if (playerItem != null) {
				TextView playerName = (TextView) v
						.findViewById(R.id.player_name);
				TextView points = (TextView) v.findViewById(R.id.player_name);
				TextView progress = (TextView) v.findViewById(R.id.player_progress);

				playerName.setText(playerItem.getPlayerName());
				points.setText(playerItem.getProgress());
				
				/*switch (playerItem.getImages()) {
				case 0:
					cupDrawable = context.getResources().getDrawable(
							R.drawable.no_cup);
					break;
				case 1:
					cupDrawable = context.getResources().getDrawable(
							R.drawable.cup_bronze);
					break;
				case 2:
					cupDrawable = context.getResources().getDrawable(
							R.drawable.cup_silver);
					break;
				case 3:
					cupDrawable = context.getResources().getDrawable(
							R.drawable.cup_gold);
					break;

				}

				cup.setBackground(cupDrawable);
*/
			}

			return v;
		}

	}


